/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/29/16 12:58 AM
 *  @modified 5/28/16 3:21 PM
 */

package me.iz.mobility.cleartaxassignment.controller;


import java.util.Collections;
import java.util.List;

import me.iz.mobility.cleartaxassignment.models.TweetInfoBean;

/**
 * @author ibasit
 */
public class FilterTweetsController extends BaseController {

    private final String TAG = getClass().getSimpleName();

    List<TweetInfoBean> tweets;

    public FilterTweetsController(ApiResponseListener mListener, Object input) {
        super(mListener);
        tweets = (List<TweetInfoBean>) input;
    }

    @Override
    protected void processApi() {

        Collections.sort(tweets);
        tweets = tweets.subList(0,3);
        mCallback.onSuccess(tweets);
    }



}
