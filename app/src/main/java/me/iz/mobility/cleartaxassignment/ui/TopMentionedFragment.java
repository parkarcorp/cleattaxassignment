/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/28/16 4:59 PM
 *  @modified 5/28/16 4:59 PM
 */

package me.iz.mobility.cleartaxassignment.ui;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import me.iz.mobility.cleartaxassignment.R;
import me.iz.mobility.cleartaxassignment.adapters.TopTweetListAdapter;
import me.iz.mobility.cleartaxassignment.controller.BaseController;
import me.iz.mobility.cleartaxassignment.controller.FilterTweetsController;
import me.iz.mobility.cleartaxassignment.models.TweetInfoBean;
import me.iz.mobility.cleartaxassignment.models.TweetInfoEvent;
import me.iz.mobility.cleartaxassignment.utils.BaseFragment;
import timber.log.Timber;
import tr.xip.errorview.ErrorView;


public class TopMentionedFragment extends BaseFragment implements BaseController.ApiResponseListener {

    @BindView(R.id.rvTweets)
    RecyclerView rvTweets;

    @BindView(R.id.evError)
    ErrorView evError;

    private TopTweetListAdapter mAdapter;

    public TopMentionedFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getContentView() {
        return R.layout.fragment_top_mentioned;

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void handleEvent(TweetInfoEvent tweetInfoEvent) {

        Timber.d("Event received");
        apiController = new FilterTweetsController(this,tweetInfoEvent.getTweets());
        apiController.start();

    }


    @Override
    public void onSuccess(Object obj) {

        evError.setVisibility(View.GONE);
        rvTweets.setVisibility(View.VISIBLE);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvTweets.setLayoutManager(layoutManager);
        List<TweetInfoBean> tweets = (List<TweetInfoBean>) obj;
        mAdapter = new TopTweetListAdapter(tweets);
        rvTweets.setAdapter(mAdapter);

    }

    @Override
    public void onFailure(String errorMessage) {

        evError.setVisibility(View.VISIBLE);
        evError.setSubtitle(errorMessage);
        rvTweets.setVisibility(View.GONE);

    }
}
