/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/26/16 11:50 AM
 *  @modified 5/26/16 11:46 AM
 */

package me.iz.mobility.cleartaxassignment.rest;

import me.iz.mobility.cleartaxassignment.models.Token;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface OauthService {

    @Headers({
            "Content-Type: application/x-www-form-urlencoded;charset=UTF-8",
            "Authorization: Basic dm9VYmpjQjFzRFRVbWN6dnZjMWZjVWxKYjpyZFVPRHh4TWtBMVZWOUp2bzQyZVluMnQweGJaU1htM1UzeVhFbVY1WWJBQnJTWlp1aw=="
    })
    @POST("token")
    Observable<Token> getAccessToken(@Body String body);
}