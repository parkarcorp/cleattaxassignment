/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/28/16 4:58 PM
 *  @modified 5/28/16 4:58 PM
 */

package me.iz.mobility.cleartaxassignment.ui;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import me.iz.mobility.cleartaxassignment.R;
import me.iz.mobility.cleartaxassignment.adapters.TweetListAdapter;
import me.iz.mobility.cleartaxassignment.controller.AuthenticationController;
import me.iz.mobility.cleartaxassignment.controller.BaseController;
import me.iz.mobility.cleartaxassignment.controller.TweetsController;
import me.iz.mobility.cleartaxassignment.models.TweetInfoBean;
import me.iz.mobility.cleartaxassignment.models.TweetInfoEvent;
import me.iz.mobility.cleartaxassignment.utils.BaseFragment;
import timber.log.Timber;
import tr.xip.errorview.ErrorView;


public class TweetsFragment extends BaseFragment implements BaseController.ApiResponseListener {

    @BindView(R.id.rvTweets)
    RecyclerView rvTweets;

    @BindView(R.id.pbProgress)
    ProgressBar progressBar;

    @BindView(R.id.evError)
    ErrorView evError;

    private TweetListAdapter mAdapter;

    public TweetsFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getContentView() {
        return R.layout.fragment_tweets;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater,container,savedInstanceState);

        callServerForAuthentication();

        return view;
    }

    @Override
    public void onSuccess(Object obj) {

        Timber.d("Successfully fetched tweets");
        progressBar.setVisibility(View.GONE);
        rvTweets.setVisibility(View.VISIBLE);
        evError.setVisibility(View.GONE);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvTweets.setLayoutManager(layoutManager);
        List<TweetInfoBean> tweets = (List<TweetInfoBean>) obj;
        Timber.d("Before setting on UI");
        for (TweetInfoBean tweet : tweets) {
            Timber.i("Mention count %d -- %s",tweet.getMentions(), tweet.getUserName());
        }
        mAdapter = new TweetListAdapter(tweets);
        rvTweets.setAdapter(mAdapter);
        EventBus.getDefault().postSticky(new TweetInfoEvent(tweets));

    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        rvTweets.setVisibility(View.GONE);
        evError.setVisibility(View.VISIBLE);
        evError.setTitle(errorMessage);
        evError.showRetryButton(true);
        evError.setOnRetryListener(this::callServer);

    }

    /**
     * Method to call server to fetch tweets
     */
    private void callServer() {
        Timber.d("Trying to fetch tweets..");
        apiController = new TweetsController(this,null);
        apiController.start();
    }

    /**
     * Method to call server for authentication
     */
    private void callServerForAuthentication() {

        Timber.d("Trying to get api key...");

        apiController = new AuthenticationController(new BaseController.ApiResponseListener() {
            @Override
            public void onSuccess(Object obj) {
                Timber.d("Successfully obtained api key");
                callServer();
            }

            @Override
            public void onFailure(String errorMessage) {
                progressBar.setVisibility(View.GONE);
                rvTweets.setVisibility(View.GONE);
                evError.setVisibility(View.VISIBLE);
                evError.setTitle(errorMessage);
                evError.showRetryButton(true);
                evError.setOnRetryListener(() -> callServerForAuthentication());

            }
        }, null);
        apiController.start();
    }

}
