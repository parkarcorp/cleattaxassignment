/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/28/16 4:08 PM
 *  @modified 5/28/16 4:08 PM
 */

package me.iz.mobility.cleartaxassignment.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import me.iz.mobility.cleartaxassignment.R;
import me.iz.mobility.cleartaxassignment.ui.TopMentionedFragment;
import me.iz.mobility.cleartaxassignment.ui.TweetsFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private static final int[] TITLES = {R.string.tab_tweets, R.string.tab_top_mentioned};

    private static List<Class<? extends Fragment>> fragmentList = new ArrayList<>();

    static {
        fragmentList.add(TweetsFragment.class);
        fragmentList.add(TopMentionedFragment.class);
    }

    private Context mContext;

    public SectionsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }


    @Override
    public Fragment getItem(int position) {
        return Fragment.instantiate(mContext, fragmentList.get(position).getName());
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(TITLES[position]);
    }
}