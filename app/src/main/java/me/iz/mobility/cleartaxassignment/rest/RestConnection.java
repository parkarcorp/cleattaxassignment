/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/26/16 10:43 AM
 *  @modified 5/26/16 10:28 AM
 */

package me.iz.mobility.cleartaxassignment.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.iz.mobility.cleartaxassignment.rest.converter.DualConvertorFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Create Rest Connection object to call rest API's from server
 */
public class RestConnection {

    private static Retrofit restAdapter = null;

    private final static String baseUrl = "https://api.twitter.com/1.1/search/";

    private final static String baseTokenUrl = "https://api.twitter.com/oauth2/";


    /**
     * Create rest Adapter object to call CarIQ Api Server
     *
     * @return RestAdapter Object
     */
    static Retrofit getRestAdapter() {

        if (restAdapter == null) {
            restAdapter = connectToTwitter();
        }
        return restAdapter;
    }

    public static TwitterService createTwitterService() {
        return getRestAdapter().create(TwitterService.class);
    }

    public static OauthService createAuthService() {
        return  getOAuthToken().create(OauthService.class);
    }



    private static Retrofit connectToTwitter() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        final Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping().create();

        final GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gson);

        return new Retrofit.Builder()
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }


    private static Retrofit getOAuthToken() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();


        return new Retrofit.Builder()
                .addConverterFactory(DualConvertorFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseTokenUrl)
                .client(okHttpClient)
                .build();

    }
}