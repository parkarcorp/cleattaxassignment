/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/26/16 11:10 PM
 *  @modified 5/26/16 11:10 PM
 */

package me.iz.mobility.cleartaxassignment.controller;


import me.iz.mobility.cleartaxassignment.models.TweetInfoBean;
import me.iz.mobility.cleartaxassignment.models.TwitterApiResponse;
import me.iz.mobility.cleartaxassignment.models.User;
import me.iz.mobility.cleartaxassignment.rest.RestConnection;
import me.iz.mobility.cleartaxassignment.rest.UnauthorizedException;
import me.iz.mobility.cleartaxassignment.utils.AppConstants;
import me.iz.mobility.cleartaxassignment.utils.Preferences;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static me.iz.mobility.cleartaxassignment.rest.RestErrorHandler.handleError;

/**
 * @author ibasit
 */
public class TweetsController extends BaseController {

    private final String TAG = getClass().getSimpleName();

    public TweetsController(ApiResponseListener mListener, Object input) {
        super(mListener);
    }

    @Override
    protected void processApi() {

        String authToken = "bearer "+Preferences.getString(AppConstants.TOKEN);
        subscriptions.add(getTweets(authToken)
                .concatMapIterable(TwitterApiResponse::getStatuses) // emit one tweet at a time
                .concatMap(tweet -> { // transforming for UI
                    final User user = tweet.getUser();
                    final int userMentionCount = tweet.getEntities().getUserMentions().size();
                    Timber.d("Mention count %d -- %s",userMentionCount, user.getName());
                    TweetInfoBean tweetInfo = new TweetInfoBean(user.getProfileImageUrl(),
                            user.getName(),"@"+user.getScreenName(),tweet.getText(),userMentionCount);

                    return Observable.just(tweetInfo);
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tweetsResponse -> {

                    mCallback.onSuccess(tweetsResponse);
                }, throwable -> {
                    try {
                        handleError(throwable);
                    } catch (UnauthorizedException ex) {
                        Timber.e(ex.getMessage());
                        mCallback.onFailure(ex.getMessage());
                    }
                }));

    }


    private Observable<TwitterApiResponse> getTweets(String auth) {
        return RestConnection.createTwitterService().getTweets(auth);
    }
}
