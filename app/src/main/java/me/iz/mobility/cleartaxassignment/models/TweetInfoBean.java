/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/27/16 5:50 PM
 *  @modified 5/27/16 5:50 PM
 */

package me.iz.mobility.cleartaxassignment.models;

/**
 * @author ibasit
 */

public class TweetInfoBean implements Comparable<TweetInfoBean> {

    private String profilePic;

    private String userName;

    private String userHandle;

    private String tweet;

    private int mentions;

    public TweetInfoBean(String profilePic, String userName, String userHandle, String tweet, int mentions) {
        this.profilePic = profilePic;
        this.userName = userName;
        this.userHandle = userHandle;
        this.tweet = tweet;
        this.mentions = mentions;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserHandle() {
        return userHandle;
    }

    public void setUserHandle(String userHandle) {
        this.userHandle = userHandle;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public int getMentions() {
        return mentions;
    }

    public void setMentions(int mentions) {
        this.mentions = mentions;
    }

    @Override
    public int compareTo(TweetInfoBean another) {
        return another.getMentions() - this.mentions;
    }
}
