/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/26/16 10:43 AM
 *  @modified 5/26/16 10:33 AM
 */

package me.iz.mobility.cleartaxassignment.controller;


import me.iz.mobility.cleartaxassignment.models.Token;
import me.iz.mobility.cleartaxassignment.rest.RestConnection;
import me.iz.mobility.cleartaxassignment.rest.UnauthorizedException;
import me.iz.mobility.cleartaxassignment.utils.AppConstants;
import me.iz.mobility.cleartaxassignment.utils.Preferences;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static me.iz.mobility.cleartaxassignment.rest.RestErrorHandler.handleError;

/**
 * @author ibasit
 */
public class AuthenticationController extends BaseController {

    private final String TAG = getClass().getSimpleName();

    AuthenticationController authRequest;


    public AuthenticationController(ApiResponseListener mListener, Object input) {
        super(mListener);

        authRequest = (AuthenticationController) input;
    }

    @Override
    protected void processApi() {

        subscriptions.add(getAuthToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(token -> {
                    Timber.d("Obtained token %s", token.toString());
                    Preferences.saveString(AppConstants.TOKEN, token.getAccessToken());
                    mCallback.onSuccess(null);
                }, throwable -> {
                    try {
                        handleError(throwable);
                    } catch (UnauthorizedException ex) {
                        Timber.e(ex.getMessage());
                        mCallback.onFailure(ex.getMessage());
                    }
                }));

    }


    private Observable<Token> getAuthToken() {
        return RestConnection.createAuthService().getAccessToken("grant_type=client_credentials");
    }
}
