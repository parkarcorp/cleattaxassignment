/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 5/27/16 5:50 PM
 *  @modified 5/27/16 5:52 PM
 */

package me.iz.mobility.cleartaxassignment.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.iz.mobility.cleartaxassignment.R;
import me.iz.mobility.cleartaxassignment.models.TweetInfoBean;


/**
 * Created by ibasit on 11/15/15.
 */
public class TweetListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TweetInfoBean> tweets;

    public TweetListAdapter(List<TweetInfoBean> tweets) {
        this.tweets = tweets;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v1 = inflater.inflate(R.layout.list_item_tweets, parent, false);
        viewHolder = new TweetListViewHolder(v1);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        TweetListViewHolder viewHolder = (TweetListViewHolder) holder;
        TweetInfoBean tweet = tweets.get(position);

        Picasso.with(viewHolder.ivProfilePic.getContext()).
                load(tweet.getProfilePic()).fit().centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.ivProfilePic);
        viewHolder.tvTweet.setText(tweet.getTweet());
        viewHolder.tvUserHandle.setText(tweet.getUserHandle());
        viewHolder.tvUserName.setText(tweet.getUserName());

    }

    @Override
    public int getItemCount() {
        return tweets != null ? tweets.size() : 0;
    }

    static class TweetListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivProfilePic)
        ImageView ivProfilePic;

        @BindView(R.id.tvTweet)
        TextView tvTweet;

        @BindView(R.id.tvUserName)
        TextView tvUserName;

        @BindView(R.id.tvUserHandle)
        TextView tvUserHandle;


        TweetListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
